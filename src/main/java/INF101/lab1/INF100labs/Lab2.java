package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        ArrayList<String> words = new ArrayList<String>();
        words.add(word1);
        words.add(word2);
        words.add(word3);

        int longestWordLength = Integer.MIN_VALUE;
        for (String i : words) {
            if (i.length() > longestWordLength) {
                longestWordLength = i.length();
            }
        }
        for (String i : words) {
            if (i.length() == longestWordLength) {
                System.out.println(i);
            }
        }
    }

    public static boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0 || year % 400 == 0);
    }

    public static boolean isEvenPositiveInt(int num) {
        return (num > 0 && num % 2 == 0);
    }

}
