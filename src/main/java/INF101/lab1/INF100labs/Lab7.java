package INF101.lab1.INF100labs;

import java.util.ArrayList;
/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int[] rowSum = new int[grid.size()];
        int[] colSum = new int[grid.size()];
        for(int i = 0; i < grid.size(); i++){
            for(int j = 0; j < grid.size(); j++){
                rowSum[i] = rowSum[i] + grid.get(i).get(j);
                colSum[j] = colSum[j] + grid.get(i).get(j);
            }
        }
        for(int i = 1; i < grid.size(); i++){
            if(rowSum[i] != rowSum[i - 1] || colSum[i] != colSum[i - 1]){
                return false;
            }
        }
        return true;
    }

}