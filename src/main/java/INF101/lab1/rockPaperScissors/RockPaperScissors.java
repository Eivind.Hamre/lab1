package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    private int getValidInput(String prompt, List<String> a) {
        String input = readInput(prompt);
        int inputAsInt = a.indexOf(input);
        if (inputAsInt == -1) {
            System.out.printf("I do not understand %s. Could you try again?\n", input);
            return getValidInput(prompt, a);
        }
        return inputAsInt;
    }

    public void run() {
        Random rng = new Random();
        do {
            System.out.printf("Let's play round %d\n", roundCounter++);
            int computer = rng.nextInt(0, 3);
            int human = getValidInput("Your choice (Rock/Paper/Scissors)?", rpsChoices);

            System.out.printf("Human chose %s, computer chose %s. ",
                    rpsChoices.get(human), rpsChoices.get(computer));
            switch ((3 + computer - human) % 3) {
                case 0: {
                    System.out.println("It's a tie!");
                    break;
                }
                case 1: {
                    computerScore++;
                    System.out.println("computer wins!");
                    break;
                }
                case 2: {
                    humanScore++;
                    System.out.println("Human wins!");
                    break;
                }
            }
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
        } while (getValidInput("Do you wish to continue playing? (y/n)?", Arrays.asList("y", "n")) == 0);
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}